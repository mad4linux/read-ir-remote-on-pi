#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <linux/uinput.h>


int openVirtKeyBrd() {
	// get real user id (who are you?) and the effective uid (who do you want to be now?)
  	uid_t ruid = getuid ();
  	uid_t euid = geteuid ();
	int status;
	int virtKey_fd, loopcount = 0;
	//become root
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (euid);
	#else
  		status = setreuid (ruid, euid);
	#endif
  	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    		return 0;
    	}
	while (virtKey_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK) == -1) {
		if (loopcount == 0) {
			printf("waiting for /dev/uinput\n");
			++loopcount;
		}
		else {
			printf(".");
		}
	}

	ioctl(virtKey_fd, UI_SET_EVBIT, EV_KEY);
	// define keys that may be sent
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_5);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_6);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_7);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_8);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_9);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_0);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_SPACE); 
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ENTER);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ESC);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFTCTRL);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_UP);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_DOWN);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_RIGHT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_X);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_M);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_PAUSE);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_MUTE);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEDOWN); 
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEUP);
	//ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_);
	struct uinput_setup usetup; // struct defined in usetup.h
   	memset(&usetup, 0, sizeof(usetup));
   	usetup.id.bustype = BUS_USB;
   	usetup.id.vendor = 0x0004;
   	usetup.id.product = 0xa1b2;
   	strcpy(usetup.name, "Remote control");

   	ioctl(virtKey_fd, UI_DEV_SETUP, &usetup);
   	ioctl(virtKey_fd, UI_DEV_CREATE);
	// become unprivileged user again
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (ruid);
	#else
  		status = setreuid (euid, ruid);
	#endif
	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    	}
   	return virtKey_fd;
}



void sendKeyPress(int fd, int type, int code, int val)
{
   struct input_event event;
   event.type = type;
   event.code = code;
   event.value = val;
   // timestamp values below are ignored
   event.time.tv_sec = 0;
   event.time.tv_usec = 0;
   write(fd, &event, sizeof(event));
}

void prepareKeyPress(unsigned int keycodes[2], int virtKeyfd) {
	printf("and the keycodes are ... %i : %i\n",keycodes[0], keycodes[1]);
	if (keycodes[1] == 0) {
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 1);  // key on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 0);  // key off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
	}
	else {
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 1);  // key 1 on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[1], 1);  // key 2 on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[1], 0);  // key 2 off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 0);  // key 1 off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
	}
}


int main(int argc, char *argv[]) {
	int virtKeyfd = openVirtKeyBrd();
	sleep(1);
	unsigned int validKeyCodes[2];
	validKeyCodes[1] = 0;
	for (int i = 1; i < 20 ; ++i) {
		validKeyCodes[0] = i;
		prepareKeyPress(validKeyCodes, virtKeyfd);
		sleep(1);
	}
	sleep(1);
	ioctl(virtKeyfd, UI_DEV_DESTROY);
	close(virtKeyfd);
	return 0;
}
