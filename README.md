Simple tool to read a infrared remote controller attached to a raspberry Pi's GPIO pins.
Inputs are forwarded as key presses to any application running on the system.

The working version is:
readRemoteLibevdev.c

The rest is testing versions and an older version without using libevev in readRemote.c which is not working at all.
Binaries are compiled on raspberry 

Detailed description

read signals from a remote control on gpio pin 4

emulate a usb keyboard and send keys pressed on remote

this program must be owned by root and setuid bit set:

 sudo chown root:pi readRemoteEvdev
 
 sudo chmod 4755 readRemoteEvdev
 
root privileges will be dropped as soon as the 
emulated keyboard is set up
