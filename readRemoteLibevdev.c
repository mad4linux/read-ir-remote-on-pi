/********************************************************
* read signals from a remote control on gpio pin 4
* emulate a usb keyboard and send keys pressed on remote
* this program must be owned by root and setuid bit set:
* sudo chown root:pi readRemoteEvdev
* sudo chmod 4755 readRemoteEvdev
* root privileges will be dropped as soon as the
* emulated keyboard is set up
********************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <linux/uinput.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>


//!! power button causes troubles

int openGPIO() {
	const char* syspath = "/sys/class/gpio/";
        const char* gpioExportPath = "/sys/class/gpio/export";
        const char* gpioEdge = "/sys/class/gpio/gpio4/edge";
        const char* gpioValues = "/sys/class/gpio/gpio4/value";
	int fd, valuefd, loopcount = 0;

	while ((fd = open(gpioExportPath, O_WRONLY)) == -1) {
		if (loopcount == 0) {
			printf("waiting for file %s", gpioExportPath);
			++loopcount;
		} else {
			printf(".");
		}
	}
	write(fd, "4", 1);
	close(fd);
	loopcount = 0;
        while ((fd = open(gpioEdge, O_WRONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioEdge);
			++loopcount;
		} else {
			printf(".");
		}
	}
        write(fd, "both", 4);
        close(fd);
        loopcount = 0;
	while ((fd = open(gpioValues, O_RDONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioValues);
			++loopcount;
		} else {
			printf(".");
		}
	}
	return fd;
}

void closeGPIO() {
	int fd, loopcount = 0;
        const char* gpioUnexportPath = "/sys/class/gpio/unexport";
	while ((fd = open(gpioUnexportPath, O_WRONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioUnexportPath);
			++loopcount;
		} else {
			printf(".");
		}
	}
        write(fd, "4", 1);
        close(fd);
}

/********************************
* HELLO1 _-_-__--
* HELLO2 _-__-_--
* Code  18 - 19 (+-)
*********************************/

void displaySignal(char *state, int delta) {
	if (delta > 1000) {
		printf("\n");
		return;
	}
	if (delta > 50) {
		printf("   |   ");
		return;
	}
	char signal = state[0];
	if (signal == '1') {
		if (delta > 15) {
			printf("--");
			return;
		}
		if (delta > 5) {
			printf("-");
			return;
		}
		return;
	}
	if (signal == '0') {
		if (delta > 15) {
			printf("__");
			return;
		}
		if (delta > 5) {
			printf("_");
			return;
		}
		return;
	}
}

void encodeSignal(char *state, int delta, unsigned short int * counter, unsigned int * signal, unsigned short int *cutBits) {
	if (delta > 1000) {
		*counter = 0;
		*signal = 0;
		return;
	}
	if (delta > 50) {
		//!! second signal
		*counter = 0;
		*cutBits = 1;
		return;
	}
	char pinValue = state[0];
	if (pinValue == '1') {
		if (delta > 15) {
			*signal += 1 << *counter - *cutBits;
			++*counter;
			*signal += 1 << *counter - *cutBits;
			return;
		}
		if (delta > 5) {
			*signal += 1 << *counter - *cutBits;
			return;
		}
	}
	else {  // works better when ignoring 0 lenght
		if (delta > 15) {
			++*counter;
			return;
		}
	}
}

int checkHello(unsigned short int * counter, unsigned int * signal, unsigned short int *cutBits) {
	if (*signal == 10) {
		*cutBits = 6;
		*signal = 0;
		return 1;
	}
	if (*signal == 12) {
		*cutBits = 6;
		*signal = 0;
		return 1;
	}
	if (*signal == 16) {
		*cutBits = 6;
		*signal = 0;
		return 1;
	}
	//printf("\ngarbage, counter = %i, signal = %i\n", *counter, *signal);
	*cutBits = 1;
	*signal = 0;
	return 0;
}

void lookUpKeyCodes(unsigned int signal, unsigned int *keycodes) {
	#define BUTTONS 33
	int keyCodeArray[BUTTONS][3] = {
	{1665638, KEY_MUTE, 0}, {677478, KEY_VOLUMEDOWN, 0}, {1484390, KEY_VOLUMEUP, 0}, {617062, KEY_ESC, 0},
	{1747558, KEY_1, 0}, {436838, KEY_2, 0}, {1485414, KEY_3, 0}, {1481318, KEY_LEFTCTRL, KEY_F1},
	{633446, KEY_4, 0}, {1682022, KEY_5, 0}, {371302, KEY_6, 0}, {420454, 0, 0},
	{1419878, KEY_7, 0}, {682598, KEY_8, 0}, {1731174, KEY_9, 0}, {698982, KEY_0, 0},
	{1403494, KEY_NEXT, 0}, {697958, KEY_UP, 0}, {432742, KEY_M, 0},
	{1743462, KEY_LEFT, 0}, {1415782, KEY_ENTER, 0}, {694886, KEY_RIGHT, 0},
	{1402470, 0, 0}, {1746534, KEY_DOWN, 0}, {435814, 0, 0},
	{1468006, 0, 0}, {616038, 0, 0}, {1664614, 0, 0}, {353894, 0, 0},
	{366182, KEY_PREVIOUS, 0}, {611942, KEY_STOPCD, 0}, {1730150, KEY_PLAY, 0}, {1403494, KEY_NEXT, 0}
	}; // "info" and "hilfe" buttons generate same key code
	//printf("lookUpKeyCodes signal = %i\n", signal);
	for (int lookupindex = 0; lookupindex < BUTTONS; ++lookupindex) {
		if (keyCodeArray[lookupindex][0] == signal) {
			keycodes[0] = keyCodeArray[lookupindex][1];
			keycodes[1] = keyCodeArray[lookupindex][2];
			return;
		}
	}
	keycodes[0] = 0;
	keycodes[1] = 0;
}


int examineSignal(unsigned int *remoteCodes, unsigned int *validKeyCodes) {
	unsigned int keyCodes[2];
	//printf("examineSignal remoteCodes[0] = %i, remoteCodes[1] = %i\n", remoteCodes[0], remoteCodes[1]); 
	lookUpKeyCodes(remoteCodes[0], keyCodes);
	if (remoteCodes[0] != remoteCodes[1]) {
		unsigned int secondKeyCodes[2];
		lookUpKeyCodes(remoteCodes[1], secondKeyCodes);
		if (keyCodes[0] != 0) {
			if (secondKeyCodes[0] != 0) {
				// two different valid keyCodes received, signal was garbage
				return 0;
			}
			else {
				// first key code was valid, use it
				validKeyCodes[0] = keyCodes[0];
				validKeyCodes[1] = keyCodes[1];
				return 1;
			}
		}
		else {
			if (secondKeyCodes[0] != 0) {
				// two different valid keyCodes received, signal was garbage
				return 0;
			}
			else {
				// second key code was valid, use it
				validKeyCodes[0] = secondKeyCodes[0];
				validKeyCodes[1] = secondKeyCodes[1];
				return 1;
			}
		}
	}
	validKeyCodes[0] = keyCodes[0];
	validKeyCodes[1] = keyCodes[1];
	return 1;
}

int openVirtKeyBrd(struct libevdev_uinput** uindev) {
	int errCode;
	struct libevdev *skeletonDev;
	skeletonDev = libevdev_new();
	libevdev_set_name(skeletonDev, "virtual keyboard for remote input");
	libevdev_enable_event_type(skeletonDev, EV_KEY);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_1, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_2, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_3, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_4, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_5, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_6, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_7, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_8, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_9, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_0, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_SPACE, NULL); 
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_ENTER, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_ESC, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_LEFTCTRL, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_F1, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_F2, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_F3, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_F4, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_UP, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_DOWN, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_LEFT, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_RIGHT, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_X, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_M, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_MUTE, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_VOLUMEDOWN, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_VOLUMEUP, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_PLAY, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_STOPCD, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_PREVIOUS, NULL);
	libevdev_enable_event_code(skeletonDev, EV_KEY, KEY_NEXT, NULL);
	// get real user id (who are you?) and the effective uid (who do you want to be now?)
  	uid_t ruid = getuid ();
  	uid_t euid = geteuid ();
	int virtKey_fd, loopcount = 0;
	//become root
	#ifdef _POSIX_SAVED_IDS
  		errCode = seteuid (euid);
	#else
  		errCode = setreuid (ruid, euid);
	#endif
  	if (errCode < 0) {
    		printf ("Couldn't set uid.\n");
    		return 0;
    	}
	errCode = libevdev_uinput_create_from_device(skeletonDev, LIBEVDEV_UINPUT_OPEN_MANAGED, uindev);
	if (errCode != 0)
		return errCode;
	// become unprivileged user again
	#ifdef _POSIX_SAVED_IDS
  		errCode = seteuid (ruid);
	#else
  		errCode = setreuid (euid, ruid);
	#endif
	if (errCode < 0) {
    		printf ("Couldn't set uid.\n");
    	}
	return 0;
}
/*int openVirtKeyBrd() {
	// get real user id (who are you?) and the effective uid (who do you want to be now?)
  	uid_t ruid = getuid ();
  	uid_t euid = geteuid ();
	int status;
	int virtKey_fd, loopcount = 0;
	//become root
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (euid);
	#else
  		status = setreuid (ruid, euid);
	#endif
  	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    		return 0;
    	}
	while (virtKey_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK) == -1) {
		if (loopcount == 0) {
			printf("waiting for /dev/uinput\n");
			++loopcount;
		}
		else {
			printf(".");
		}
	}

	ioctl(virtKey_fd, UI_SET_EVBIT, EV_KEY);
	// define keys that may be sent
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_5);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_6);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_7);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_8);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_9);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_0);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_SPACE); 
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ENTER);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ESC);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFTCTRL);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_UP);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_DOWN);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_RIGHT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_X);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_M);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_MUTE);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEDOWN);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEUP);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_PAUSE);
	//ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_);
	struct uinput_setup usetup; // struct defined in usetup.h
   	memset(&usetup, 0, sizeof(usetup));
   	usetup.id.bustype = BUS_USB;
   	usetup.id.vendor = 0x0004;
   	usetup.id.product = 0xa1b2;
   	strcpy(usetup.name, "Remote control");

   	ioctl(virtKey_fd, UI_DEV_SETUP, &usetup);
   	ioctl(virtKey_fd, UI_DEV_CREATE);
	// become unprivileged user again
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (ruid);
	#else
  		status = setreuid (euid, ruid);
	#endif
	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    	}
   	return virtKey_fd;
}*/
int sendKeyPress(struct libevdev_uinput*  uindev, int type, int code, int val) {
	int errCode = libevdev_uinput_write_event(uindev, type, code, val);
	if (errCode != 0)
		return errCode;
	return 0;
}

/*void sendKeyPress(int fd, int type, int code, int val)
{
   struct input_event event;
   event.type = type;
   event.code = code;
   event.value = val;
   // timestamp values below are ignored
   event.time.tv_sec = 0;
   event.time.tv_usec = 0;
   write(fd, &event, sizeof(event));
}*/

void prepareKeyPress(unsigned int keycodes[2], struct libevdev_uinput*  virtKeyDev) {
	printf("and the keycodes are ... %i : %i\n",keycodes[0], keycodes[1]);
	if (keycodes[1] == 0) {
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[0], 1);  // key on
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);   // report
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[0], 0);  // key off
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);   // report
	}
	else {
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[0], 1);  // key 1 on
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[1], 1);  // key 2 on
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[1], 0);  // key 2 off
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyDev, EV_KEY, keycodes[0], 0);  // key 1 off
		sendKeyPress(virtKeyDev, EV_SYN, SYN_REPORT, 0);
	}
}

int main(int argc, char *argv[]) {
	int gpiofd = openGPIO();
	struct libevdev_uinput* uinputdev;
	int errCode = openVirtKeyBrd(&uinputdev);
	if (errCode)
		printf("\ncannot create uiput, error: %i\n", errCode);
	// write a test event
	errCode = libevdev_uinput_write_event(uinputdev, EV_KEY, KEY_1, 1);
	if (errCode != 0)
		return errCode;
	errCode = libevdev_uinput_write_event(uinputdev, EV_SYN, SYN_REPORT, 0);
	if (errCode != 0)
		return errCode;
		
	struct pollfd linuxpoll[1];
	nfds_t nrfds = 1;
	memset(linuxpoll, 0 , sizeof(linuxpoll));
	linuxpoll[0].fd = gpiofd;
	linuxpoll[0].events = POLLPRI + POLLERR;
	printf("\nstart polling\n");
	char gpiovalue[5] = {0};
	char runningstate[2];
	double timebuf; 
	int newtime = 0, oldtime = 0, delta = 0;
	unsigned int timereturn, signal = 0, remoteCodes[2];
	unsigned short int loopcounter = 0, incoming = 0, cutBits = 1, helloACK;
	struct timespec myts[1];
	for (;;) {
		if (incoming) {
			poll(linuxpoll, nrfds, 100);
			++loopcounter;
		} 
		else {
			poll(linuxpoll, nrfds, -1);
			helloACK = 0;
			incoming = 1;
			loopcounter = 0;
			cutBits = 1;
			signal = 0;
		}
		if (linuxpoll[0].revents & POLLPRI) {
			lseek(gpiofd, 0, SEEK_SET);
			size_t rs = read(gpiofd, gpiovalue, 1);
			runningstate[0] = runningstate[1];
			runningstate[1] = gpiovalue[0];
			timereturn = clock_gettime(CLOCK_MONOTONIC, myts);
			oldtime = newtime;
			int significantfraction = myts[0].tv_nsec / 100000;  // int division
			timebuf = myts[0].tv_sec + significantfraction / 10000.0; // float division
			newtime = timebuf * 10000;
			delta = newtime - oldtime;
			displaySignal(runningstate, delta); 			
			encodeSignal(runningstate, delta, &loopcounter, &signal, &cutBits);
			//printf("counter = %i, signal = %i, cutBits = %i\n", loopcounter, signal, cutBits);
			if (loopcounter == 0 && helloACK) {
				printf("signal = %i\n", signal);
				remoteCodes[0] = signal;
				signal = 0;
				helloACK = 0;
			}
			if (!helloACK && ((loopcounter == 4) || (loopcounter == 5))) {
				helloACK = checkHello(&loopcounter, &signal, &cutBits);
			}
			//printf("pin value = %s at ", gpiovalue);
			//printf("%u.%u (leading zeros omitted) sec: delta = %i\n", myts[0].tv_sec, myts[0].tv_nsec, newtime - oldtime);
		}
		else if (linuxpoll[0].revents & POLLERR) {
			printf("\nERROR\n");
			closeGPIO();
			return 1;
		}
		else {
			if (helloACK) {
				printf("signal = %i\n", signal);
				remoteCodes[1] = signal;
				//printf("main if(helloACK) remoteCodes[1] = %i, signal = %i\n", remoteCodes[1], signal);
				signal = 0;
				helloACK = 0;
				unsigned int keyCodes[2];
				if (examineSignal(remoteCodes, keyCodes)) {
					//printf("main keyCodes[0] = %i, keyCodes[1] = %i", keyCodes[0], keyCodes[1])
					prepareKeyPress(keyCodes, uinputdev);
				}
			}
			incoming = 0;
		}
	}
	return 0;
}
