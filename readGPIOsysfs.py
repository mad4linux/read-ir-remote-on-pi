#!/usr/bin/python3

import os
os.chdir('/sys/class/gpio')
with open('export','w') as exportfile:
  exportfile.write('4')
  exportfile.close()

os.chdir('gpio4')
count = 0
displayline = ''
try:
  while(1==1):
    valuefile = open('value','r')
    valueline = valuefile.read()
    valuefile.close()
    valuestr, newline = valueline
    if (valuestr == '0') or (count > 0):
      count += 1
      displayline += valuestr
    if (count > 32):
      count = 0
      print ("signal: " + displayline)
except KeyboardInterrupt:
  valuefile.close()
  pass

print("reading gpio 4 finished")
os.chdir('/sys/class/gpio')
with open('unexport','w') as unexportfile:
  unexportfile.write('4')
  unexportfile.close()

