#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <linux/input.h>
#include <linux/uinput.h>

int openVirtKeyBrd() {
	// get real user id (who are you?) and the effective uid (who do you want to be now?)
  	uid_t ruid = getuid ();
  	uid_t euid = geteuid ();
	int status;
	int virtKey_fd, loopcount = 0;
	//become root
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (euid);
	#else
  		status = setreuid (ruid, euid);
	#endif
  	if (status < 0) {
    		printf ("Couldn't set uid.: %s\n", strerror(errno));
    		return 0;
    	}
	while (virtKey_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK) == -1) {
		if (loopcount == 0) {
			printf("waiting for /dev/uinput\n");
			++loopcount;
		}
		else {
			sleep(1);
			printf(".");
		}
	}
	sleep(1);  //doesn't help
	status = ioctl(virtKey_fd, UI_SET_EVBIT, EV_KEY);
	if (status < 0) {
		printf("cannot set evbit: %s\n", strerror(errno));
	}
	// define keys that may be sent
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_1);
	if (status < 0) {
		printf("cannot set key 1: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_2);
	if (status < 0) {
		printf("cannot set key 2: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_3);
	if (status < 0) {
		printf("cannot set key 3: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_4);
	if (status < 0) {
		printf("cannot set key 4: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_5);
	if (status < 0) {
		printf("cannot set key 5: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_6);
	if (status < 0) {
		printf("cannot set key 6: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_7);
	if (status < 0) {
		printf("cannot set key 7: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_8);
	if (status < 0) {
		printf("cannot set key 8: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_9);
	if (status < 0) {
		printf("cannot set key 9: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_0);
	if (status < 0) {
		printf("cannot set key 0: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_SPACE); 
	if (status < 0) {
		printf("cannot set key space: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ENTER);
	if (status < 0) {
		printf("cannot set key enter: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ESC);
	if (status < 0) {
		printf("cannot set key esc: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFTCTRL);
	if (status < 0) {
		printf("cannot set key left CTRL: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F1);
	if (status < 0) {
		printf("cannot set key F1: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F2);
	if (status < 0) {
		printf("cannot set key F2: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F3);
	if (status < 0) {
		printf("cannot set key F3: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F4);
	if (status < 0) {
		printf("cannot set key F4: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_UP);
	if (status < 0) {
		printf("cannot set key up: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_DOWN);
	if (status < 0) {
		printf("cannot set key down: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFT);
	if (status < 0) {
		printf("cannot set key left: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_RIGHT);
	if (status < 0) {
		printf("cannot set key right: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_X);
	if (status < 0) {
		printf("cannot set key X: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_M);
	if (status < 0) {
		printf("cannot set key M: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_MUTE);
	if (status < 0) {
		printf("cannot set key mute: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEDOWN);
	if (status < 0) {
		printf("cannot set key Vol-: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEUP);
	if (status < 0) {
		printf("cannot set key Vol+: %s\n", strerror(errno));
	}
	status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_PAUSE);
	if (status < 0) {
		printf("cannot set key pause: %s\n", strerror(errno));
	}
	//status = ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_);
	struct uinput_setup usetup; // struct defined in usetup.h
   	memset(&usetup, 0, sizeof(usetup));
   	strcpy(usetup.name, "Remote control");
   	usetup.id.bustype = BUS_VIRTUAL;
   	usetup.id.vendor = 0x1;
   	usetup.id.product = 0x1;
	usetup.id.version = 1;

/*	status = write(virtKey_fd, &usetup, sizeof(usetup));
	if (status < 0) {
		printf("cannot write setup to uinput dev: %s\n", strerror(errno));
	} */
	
   	status = ioctl(virtKey_fd, UI_DEV_SETUP, &usetup);
	if (status < 0) {
		printf("cannot setup uinput dev: %s\n", strerror(errno));
	}

	status = ioctl(virtKey_fd, UI_DEV_CREATE);
	if (status < 0) {
		printf("cannot create uinput dev: %s\n", strerror(errno));
	}
	// become unprivileged user again
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (ruid);
	#else
  		status = setreuid (euid, ruid);
	#endif
	if (status < 0) {
    		printf ("Couldn't set uid.: %s\n", strerror(errno));
    	}
   	return virtKey_fd;
}

void sendKeyPress(int fd, int type, int code, int val)
{
   struct input_event event;
   event.type = type;
   event.code = code;
   event.value = val;
   // timestamp values below are ignored
   event.time.tv_sec = 0;
   event.time.tv_usec = 0;
   int success = write(fd, &event, sizeof(event));
   printf("success = %i\n", success);
}


int main(int argc, char *argv[]) {
	int virtKeyfd = openVirtKeyBrd();
	sleep(1);
	sendKeyPress(virtKeyfd, EV_KEY, KEY_1, 1);  // key on
	sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
	sendKeyPress(virtKeyfd, EV_KEY, KEY_1, 0);  // key off
	sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
	sleep(1);
}
