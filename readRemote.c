/********************************************************
* read signals from a remote control on gpio pin 4
* emulate a usb keyboard and send keys pressed on remote
* this program must be owned by root and setuid bit set:
* sudo chown root:pi readRemote
* sudo sudo chmod 4755 readRemote
* root privileges will be dropped as soon as the
* emulated keyboard is set up
********************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <linux/uinput.h>

//!! power button causes troubles

int openGPIO() {
	const char* syspath = "/sys/class/gpio/";
        const char* gpioExportPath = "/sys/class/gpio/export";
        const char* gpioEdge = "/sys/class/gpio/gpio4/edge";
        const char* gpioValues = "/sys/class/gpio/gpio4/value";
	int fd, valuefd, loopcount = 0;

	while ((fd = open(gpioExportPath, O_WRONLY)) == -1) {
		if (loopcount == 0) {
			printf("waiting for file %s", gpioExportPath);
			++loopcount;
		} else {
			printf(".");
		}
	}
	write(fd, "4", 1);
	close(fd);
	loopcount = 0;
        while ((fd = open(gpioEdge, O_WRONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioEdge);
			++loopcount;
		} else {
			printf(".");
		}
	}
        write(fd, "both", 4);
        close(fd);
        loopcount = 0;
	while ((fd = open(gpioValues, O_RDONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioValues);
			++loopcount;
		} else {
			printf(".");
		}
	}
	return fd;
}

void closeGPIO() {
	int fd, loopcount = 0;
        const char* gpioUnexportPath = "/sys/class/gpio/unexport";
	while ((fd = open(gpioUnexportPath, O_WRONLY)) == -1) {
		if (loopcount == 0) {
		printf("waiting for file %s", gpioUnexportPath);
			++loopcount;
		} else {
			printf(".");
		}
	}
        write(fd, "4", 1);
        close(fd);
}

/********************************
* HELLO1 _-_-__--
* HELLO2 _-__-_--
* Code  18 - 19 (+-)
*********************************/

void displaySignal(char *state, int delta) {
	if (delta > 1000) {
		printf("\n");
		return;
	}
	if (delta > 50) {
		printf("   |   ");
		return;
	}
	char signal = state[0];
	if (signal == '1') {
		if (delta > 15) {
			printf("--");
			return;
		}
		if (delta > 5) {
			printf("-");
			return;
		}
		return;
	}
	if (signal == '0') {
		if (delta > 15) {
			printf("__");
			return;
		}
		if (delta > 5) {
			printf("_");
			return;
		}
		return;
	}
}

void encodeSignal(char *state, int delta, unsigned short int * counter, unsigned int * signal, unsigned short int *cutBits) {
	if (delta > 1000) {
		*counter = 0;
		*signal = 0;
		return;
	}
	if (delta > 50) {
		//!! second signal
		*counter = 0;
		*cutBits = 1;
		return;
	}
	char pinValue = state[0];
	if (pinValue == '1') {
		if (delta > 15) {
			*signal += 1 << *counter - *cutBits;
			++*counter;
			*signal += 1 << *counter - *cutBits;
			return;
		}
		if (delta > 5) {
			*signal += 1 << *counter - *cutBits;
			return;
		}
	}
	/*else {  // works better when ignoring 0 lenght
		if (delta > 15) {
			++*counter;
			return;
		}
	}*/
}

int checkHello(unsigned short int * counter, unsigned int * signal, unsigned short int *cutBits) {
	if (*signal == 10) {
		*cutBits = 5;
		*signal = 0;
		return 1;
	}
	else if (*signal == 6) {
		*cutBits = 4;
		*signal = 0;
		return 1;
	}
	else {
		//printf("\ngarbage, counter = %i, signal = %i\n", *counter, *signal);
		*cutBits = 1;
		*signal = 0;
		return 0;
	}
}

void showSignal(unsigned short int * counter, unsigned int * signal) {
	printf("signal = %i\n", *signal);
	*signal = 0;
}

void lookUpKeyCodes(unsigned int signal, unsigned int *keycodes) {
	#define BUTTONS 33
	int keyCodeArray[BUTTONS][3] = {
	{219830, KEY_MUTE, 0}, {87478, KEY_VOLUMEDOWN, 0}, {185752, KEY_VOLUMEUP, 0}, {88758, KEY_ESC, 0},
	{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
	{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
	{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
	{0, 0, 0}, {87478, KEY_UP, 0}, {54966, KEY_M, 0},
	{218806, KEY_LEFT, 0}, {177846, KEY_ENTER, 0}, {87734, KEY_RIGHT, 0},
	{0, 0, 0}, {218550, KEY_DOWN, 0}, {0, 0, 0},
	{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
	{46518, KEY_PREVIOUS, 0}, {87487, KEY_PAUSE, 0}, {110006, KEY_PLAY, 0}, {350902, KEY_NEXT, 0}
	};
	
	for (int lookupindex = 0; lookupindex < BUTTONS; ++lookupindex) {
		if (keyCodeArray[lookupindex][0] == signal) {
			keycodes[0] = keyCodeArray[lookupindex][1];
			keycodes[1] = keyCodeArray[lookupindex][2];
			return;
		}
	}
	keycodes[0] = 0;
	keycodes[1] = 0;
}


int examineSignal(unsigned int *remoteCodes, unsigned int *validKeyCodes) {
	unsigned int keyCodes[2];
	lookUpKeyCodes(remoteCodes[0], keyCodes);
	if (remoteCodes[0] != remoteCodes[1]) {
		unsigned int secondKeyCodes[2];
		lookUpKeyCodes(remoteCodes[1], secondKeyCodes);
		if (keyCodes[0] != 0) {
			if (secondKeyCodes[0] != 0) {
				// two different valid keyCodes received, signal was garbage
				return 0;
			}
			else {
				// first key code was valid, use it
				validKeyCodes[0] = keyCodes[0];
				validKeyCodes[1] = keyCodes[1];
				return 1;
			}
		}
		else {
			if (secondKeyCodes[0] != 0) {
				// two different valid keyCodes received, signal was garbage
				return 0;
			}
			else {
				// second key code was valid, use it
				validKeyCodes[0] = secondKeyCodes[0];
				validKeyCodes[1] = secondKeyCodes[1];
				return 1;
			}
		}
	}
	validKeyCodes[0] = keyCodes[0];
	validKeyCodes[1] = keyCodes[1];
	return 1;
}

int openVirtKeyBrd() {
	// get real user id (who are you?) and the effective uid (who do you want to be now?)
  	uid_t ruid = getuid ();
  	uid_t euid = geteuid ();
	int status;
	int virtKey_fd, loopcount = 0;
	//become root
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (euid);
	#else
  		status = setreuid (ruid, euid);
	#endif
  	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    		return 0;
    	}
	while (virtKey_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK) == -1) {
		if (loopcount == 0) {
			printf("waiting for /dev/uinput\n");
			++loopcount;
		}
		else {
			printf(".");
		}
	}

	ioctl(virtKey_fd, UI_SET_EVBIT, EV_KEY);
	// define keys that may be sent
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_5);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_6);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_7);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_8);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_9);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_0);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_SPACE); 
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ENTER);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_ESC);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFTCTRL);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F1);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F2);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F3);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_F4);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_UP);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_DOWN);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_LEFT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_RIGHT);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_X);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_M);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_MUTE);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEDOWN);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_VOLUMEUP);
	ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_PAUSE);
	//ioctl(virtKey_fd, UI_SET_KEYBIT, KEY_);
	struct uinput_setup usetup; // struct defined in usetup.h
   	memset(&usetup, 0, sizeof(usetup));
   	usetup.id.bustype = BUS_USB;
   	usetup.id.vendor = 0x0004;
   	usetup.id.product = 0xa1b2;
   	strcpy(usetup.name, "Remote control");

   	ioctl(virtKey_fd, UI_DEV_SETUP, &usetup);
   	ioctl(virtKey_fd, UI_DEV_CREATE);
	//?? change privileges to user
	fchmod(virtKey_fd, 00660);
	fchown(virtKey_fd, ruid, getgid());
	// become unprivileged user again
	#ifdef _POSIX_SAVED_IDS
  		status = seteuid (ruid);
	#else
  		status = setreuid (euid, ruid);
	#endif
	if (status < 0) {
    		printf ("Couldn't set uid.\n");
    	}
   	return virtKey_fd;
}

void sendKeyPress(int fd, int type, int code, int val)
{
   struct input_event event;
   event.type = type;
   event.code = code;
   event.value = val;
   // timestamp values below are ignored
   event.time.tv_sec = 0;
   event.time.tv_usec = 0;
   write(fd, &event, sizeof(event));
}

void prepareKeyPress(unsigned int keycodes[2], int virtKeyfd) {
	printf("and the keycodes are ... %i : %i\n",keycodes[0], keycodes[1]);
	if (keycodes[1] == 0) {
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 1);  // key on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 0);  // key off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);   // report
	}
	else {
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 1);  // key 1 on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[1], 1);  // key 2 on
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[1], 0);  // key 2 off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
		sendKeyPress(virtKeyfd, EV_KEY, keycodes[0], 0);  // key 1 off
		sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
	}
}

int main(int argc, char *argv[]) {
	int gpiofd = openGPIO();
	int virtKeyfd = openVirtKeyBrd();
	struct pollfd linuxpoll[1];
	nfds_t nrfds = 1;
	memset(linuxpoll, 0 , sizeof(linuxpoll));
	linuxpoll[0].fd = gpiofd;
	linuxpoll[0].events = POLLPRI + POLLERR;
	printf("\nstart polling\n");
	char gpiovalue[5] = {0};
	char runningstate[2];
	double timebuf; 
	int newtime = 0, oldtime = 0, delta = 0;
	unsigned int timereturn, signal = 0, remoteCodes[2];
	unsigned short int loopcounter = 0, incoming = 0, cutBits = 1, helloACK;
	struct timespec myts[1];
	for (;;) {
		if (incoming) {
			poll(linuxpoll, nrfds, 100);
			++loopcounter;
		} 
		else {
			poll(linuxpoll, nrfds, -1);
			helloACK = 0;
			incoming = 1;
			loopcounter = 0;
			cutBits = 1;
			signal = 0;
		}
		if (linuxpoll[0].revents & POLLPRI) {
			lseek(gpiofd, 0, SEEK_SET);
			size_t rs = read(gpiofd, gpiovalue, 1);
			runningstate[0] = runningstate[1];
			runningstate[1] = gpiovalue[0];
			timereturn = clock_gettime(CLOCK_MONOTONIC, myts);
			oldtime = newtime;
			int significantfraction = myts[0].tv_nsec / 100000;  // int division
			timebuf = myts[0].tv_sec + significantfraction / 10000.0; // float division
			newtime = timebuf * 10000;
			delta = newtime - oldtime;
			displaySignal(runningstate, delta); 			
			encodeSignal(runningstate, delta, &loopcounter, &signal, &cutBits);
			//printf("counter = %i, signal = %i, cutBits = %i\n", loopcounter, signal, cutBits);
			if (loopcounter == 0 && helloACK) {
				showSignal(&loopcounter, &signal);
				remoteCodes[0] = signal;
				helloACK = 0;
			}
			if (!helloACK && ((loopcounter == 4) || (loopcounter == 5))) {
				helloACK = checkHello(&loopcounter, &signal, &cutBits);
			}
			//printf("pin value = %s at ", gpiovalue);
			//printf("%u.%u (leading zeros omitted) sec: delta = %i\n", myts[0].tv_sec, myts[0].tv_nsec, newtime - oldtime);
		}
		else if (linuxpoll[0].revents & POLLERR) {
			printf("\nERROR\n");
			closeGPIO();
			return 1;
		}
		else {
			if (helloACK) {
				showSignal(&loopcounter, &signal);
				remoteCodes[1] = signal;
				helloACK = 0;
				unsigned int keyCodes[2];
				if (examineSignal(remoteCodes, keyCodes)) {
					prepareKeyPress(keyCodes, virtKeyfd);
				}
			}
			incoming = 0;

			// sample sendKeyPress of some keys
			/* Key press, report the event, send key release, and report again */
   			/*sendKeyPress(virtKeyfd, EV_KEY, KEY_1, 1);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
   			sendKeyPress(virtKeyfd, EV_KEY, KEY_1, 0);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
   			sendKeyPress(virtKeyfd, EV_KEY, KEY_2, 1);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
   			sendKeyPress(virtKeyfd, EV_KEY, KEY_2, 0);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
   			sendKeyPress(virtKeyfd, EV_KEY, KEY_3, 1);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);
   			sendKeyPress(virtKeyfd, EV_KEY, KEY_3, 0);
   			sendKeyPress(virtKeyfd, EV_SYN, SYN_REPORT, 0);*/
		}
	}
	return 0;
}
